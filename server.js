var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var port = 12345;

http.listen(12345, function() {
	console.log("server is listening at *:12345");
});

io.on('connection', function(socket) {
	console.log("debug: connected");

	setInterval(function() {
		socket.emit('data-stream', Math.random() * 99);
	}, 1000);

	socket.on('disconnect', function() {
		console.log("debug: disconnected");
	});
});
Solution 01
===========

Client/Server Features
----------------------

![ScreenShot](./img/Capture.PNG)

Design Flaws
------------

* Current design flaws are on setInterval. When two (2) or more clients are connected to server, there are highly chance that each of them will get different 'seed'. How to fix this?

* No simple statistical (average/minimum/maximum) computational was done.

* No data visualization. 


HOWTO Setup
-----------

$ npm install --save express socket.io

$ node server.js

# Run index.html

Author
------

hadrihl // hadrihilmi@gmail.com